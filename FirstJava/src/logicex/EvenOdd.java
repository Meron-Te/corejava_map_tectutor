package logicex;
import java.util.*;
public class EvenOdd {

	int number;
	Scanner sc;
	/*
	 * =-Assignment
	 * x=10; that means x will be assigned 10
	 * x==10//it is conditional operator which is checking value of x
	 * should be 10
	 */
	
	public void accept()
	{
		sc=new Scanner(System.in);
		for(int x=1;x<=10;x++)
		{
		System.out.println("Enter Number");
		number=sc.nextInt();
		if(number%2==0)
		{
			System.out.println("It is even number");
		}
		else
		{
			System.out.println("It is odd number");
		}
		
		}
	}
	public static void main(String[] args) {
		EvenOdd evens=new EvenOdd();
		evens.accept();
	}
}
