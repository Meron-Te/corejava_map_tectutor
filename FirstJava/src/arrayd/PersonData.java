package arrayd;

public class PersonData {
	Person p1[];
	public void accept()
	{
		p1=new Person[3];
		for(int x=0;x<p1.length;x++)
		{
			p1[x]=new Person();
			/*
			 * [person][person1][person2]
			 */
			p1[x].accept();
		}
	}
	public void displayData()
	{
		for(int x=0;x<p1.length;x++)
		{
			p1[x].display();
		}
	}
	public static void main(String[] args) {
		PersonData persond=new PersonData();
		persond.accept();
		persond.displayData();
	}

}
