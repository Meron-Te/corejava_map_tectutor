package arraydemo;

public class NamesArray {
	String names[];
	public void accept()
	{
		names=new String[5];
		names[0]="Anaita";
		names[1]="Jorden";
		names[2]="John";
		names[3]="Manik";
		names[4]="Parineeta";
	}
	public void display()
	{
		for(int x=0;x<names.length;x++)
		{
			System.out.println("Names are "+names[x]);
		}
		
	}
	public static void main(String[] args) {
		NamesArray namesa=new NamesArray();
		namesa.accept();
		namesa.display();
	}

}
