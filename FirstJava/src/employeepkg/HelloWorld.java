package employeepkg;
/*
 * public-this class can be used anywhere
 * class-its keyword to create a class
 * HelloWorld-is the name of class.
 * The class name and file name must be same
 */

public class HelloWorld {
	
	public static void main(String arg[])
	{
		System.out.println("Welcome to Java Programming language");
		
	}

}
/*public-it has to be executed by jvm thats why it is public
 * static-can be used without object also
 * void-it does not return anything
 * main-is a method name
 */
 