package arraydemo;
import java.util.*;
public class ArraySum {
	private int arr[];
	private Scanner sc;
	public void accept()
	{
		sc=new Scanner(System.in);
		System.out.println("Enter no of records you want to enter in array");
		int noofvalues=sc.nextInt();
		arr=new int[noofvalues];
		//for(int counter=0;counter<arr.length;counter++)
		int counter=0;//initialization
		while(counter<arr.length)
		{
			System.out.println("Enter values");
			arr[counter]=sc.nextInt();
			counter++;
		}
	}
	public void sum()
	{
		int total=0;
		for(int x=0;x<arr.length;x++)
		{
			total=arr[x]+total;
			System.out.println("Total is "+total);
		}
	}
	public void checkEvenOdd()
	{
		for(int x=0;x<arr.length;x++)
		{
			if(arr[x]%2==0)
			{
				System.out.println("Even number");
			}
			else
			{
				System.out.println("Odd number");
			}
		}
		
	}
	
	public static void main(String[] args) {
		ArraySum arrsum=new ArraySum();
		arrsum.accept();
		arrsum.sum();
		arrsum.checkEvenOdd();
	}

}
