package employeepkg;
import java.util.*;
public class ForLoopDemo {
	private Scanner sc;
	int number;
	public void accept()
	{
		sc=new Scanner(System.in);
		int total=0;
		for(int x=1;x<=5;x++)
		{
		System.out.println("Enter Number");
		number=sc.nextInt();
		total=total+number;
		System.out.println("Sum of five numbers are "+total);
		}
		
	}
	public static void main(String[] args) {
		ForLoopDemo fors=new ForLoopDemo();
		fors.accept();
	}

}
