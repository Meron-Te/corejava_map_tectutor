package arrayobj;
import java.util.*;
public class Employee {
	private int empid;
	private String empname;
	private double salary;
	private Scanner sc;
	public void accept()
	{
		sc=new Scanner(System.in);
		System.out.println("Enter Employee id");
		empid=sc.nextInt();
		System.out.println("Enter Employee Name");
		empname=sc.next();
		System.out.println("Enter Salary ");
		salary=sc.nextDouble();
	}
	public void display()
	{
		System.out.println("Employee id is "+empid);
		System.out.println("Employee name is "+empname);
		System.out.println("Salary is "+salary);
	}
	

}
/*
 * In java whenever u create any class, that means u r creating user defined data type.
*/