package arrayobj;

public class EmployeeData {
	Employee emparr[];
	public void acceptDetails()
	{
		emparr=new Employee[3];//Employee will be  considered as user defined data type.
		//emparr is the name of array... we can have detials of five employees.
		for(int x=0;x<emparr.length;x++)
		{
			emparr[x]=new Employee();//we have to initialize Employee object in each and every position
			//of array
			emparr[x].accept();//for each employee i am accept method
		}
	}
	public void displayDetails()
	{
		for(int x=0;x<emparr.length;x++)
		{
			emparr[x].display();
		}
	}
	public static void main(String[] args) {
		EmployeeData edata=new EmployeeData();
		edata.acceptDetails();
		edata.displayDetails();
		
	}

}
